# Drone AI Project coding and flow chart
### By: Andrew Dose
----

----
> The following information will give some direction the Drone AI project that I had emailed you earlier about. Please see the flow chart made through lucidchart named: ADoseDroneCode.pdf and the text document labeled ADoseDroneCode.txt attached inside the zip folder.

---
---- 
> As you can see from the two files I listed above, they are similar in many aspects. What differentiates them is one is a picture representation of this entire process. The other is a "shorter" definition of the project code. This shorter definition is also known as: "comment lines in the code itself". Both are in the best interest of the coder as well as the consumer.

---

> please note that this is all to make this code more easily readable. One example is that the .txt code will have more information than the .pdf. The PDF will have more significant events illustrated than the .TXT.

---

---
If there are any questions please contact me via [email](andrew.dose@smail.rasmussen.edu)